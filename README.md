# userreg

## What is this?
A proof-of-concept of a user registration HTTP service. 
See the requirements here: https://docs.google.com/document/d/1GlhDk4psz7rVcPyQhK1T_vLWOmlNqfYrg4uwovaZJSU/

## Setup

1. `conda create --name=userreg python=3.8`
2. `conda activate userreg`
4. `pip install -r requirements.txt`
6. Copy `.env.template` to `.env`, and change accordingly
7. `source .env`
8. `python main.py`

## Usage

##### Basic Auth

For the GET requests, you could simply send the request to your computer 
browser (e.g `http://localhost:8080/` or `http://localhost:8080/users`), and the 
browser would automatically ask for your credentials. You could login with any
registered credentials, unless stated otherwise. 

The default credentials are `admin` `admin` for endpoints requiring 
administrator privilege, or  `patrick.hermawan` `patrick` for endpoints 
accessible by regular users. 

If this is your first time, I would totally recommend trying this in the browser
first as a checkpoint that the web server is running normally, and the
functions are working as intended.

For other requests, you may use Postman. See 
[here](https://learning.postman.com/docs/sending-requests/authorization/#basic-auth) 
for tutorial on using Basic Auth feature for authentication with Postman.


##### Adding payload

If you are using Postman, set the `body` data to `raw` data, with the type 
`JSON`. See [here](https://learning.postman.com/docs/sending-requests/requests/#sending-body-data) 
for tutorial on sending body data with Postman.

#### GET /
*(requires any user to be logged in. See Basic Auth section above)*

Generic index page

#### GET /users
*(requires ADMIN to be logged in. See Basic Auth section above)*

List all users


#### GET /users/<id_or_username_or_email>
*(requires any user to be logged in. See Basic Auth section above)*

Get a user with specific ID, username, or email


#### POST /users
*(requires ADMIN to be logged in. See Basic Auth section above)*

Add a new user. Requires a payload such as in the following example:

```
{
	"fullname": "Akira Toriyama",
	"email": "akira.toriyama@mail.com",
	"password": "string",
	"role": "newb"
}
```

#### PUT /users
*(requires ADMIN to be logged in. See Basic Auth section above)*

Update an existing user. Requires a payload such as in the following example:

```
{
	"fullname": "Akira Toriyama",
	"email": "akira.toriyama@mail.com",
	"password": "string",
	"role": "newb"
}
```

#### DELETE /users/<id_or_email>
*(requires ADMIN to be logged in. See Basic Auth section above)*

Delete a user




