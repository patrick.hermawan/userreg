from os import getenv
import jsons

from flask import Flask, request
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash

from userreg.database.db import DB
from userreg.user_registration.users_repo import UsersRepo

app = Flask(__name__)
auth = HTTPBasicAuth()

users_repo = UsersRepo(DB())
users_repo.add(fullname="admin", email="admin@admin.com", password="admin", role="admin")
users_repo.add(fullname="Patrick Hermawan", email="patrick.hermawan@outlook.com", password="patrick", role="newb")


@auth.verify_password
def verify_password(username, password):
    if not (username and password):
        raise UserWarning("User or password does not exist")
    try:
        user = users_repo.get(username)
    except:
        raise UserWarning("User not found")
    if check_password_hash(user.password, username + password):
        return username
    else:
        raise UserWarning("Wrong password")


@auth.get_user_roles
def get_user_roles(username):
    return users_repo.get(username).role


@app.route('/')
@auth.login_required
def index():
    return "Hello, {}!".format(auth.current_user())


@app.route('/users', methods=['GET'])
@auth.login_required(role='admin')
def list_users():
    return {"status": "ok", "data": users_repo.get_all()}


@app.route('/users/<id_or_username_or_email>', methods=['GET'])
@auth.login_required()
def get_user(id_or_username_or_email):
    return {"status": "ok", "data": jsons.loads(jsons.dumps(users_repo.get(id_or_username_or_email)))}


@app.route('/users', methods=['POST'])
@auth.login_required(role='admin')
def insert_user():
    data = request.get_json()

    if not ("fullname" in data and "email" in data and "password" in data and "role" in data):
        raise Exception("Data Incomplete. Please provide fullname, email, password, and role")

    new_user = users_repo.add(
        fullname=data["fullname"],
        email=data["email"],
        password=data["password"],
        role=data["role"],
    )
    return {"status": "ok", "data": jsons.loads(jsons.dumps(new_user))}


@app.route('/users', methods=['PATCH'])
@auth.login_required(role='admin')
def update_user():
    data = request.get_json()

    if not ("email" in data or "id" in data):
        raise Exception("Please provide email or id")

    new_user = users_repo.replace(
        fullname=data["fullname"] if "fullname" in data else None,
        email=data["email"] if "email" in data else None,
        password=data["password"] if "password" in data else None,
        role=data["role"] if "role" in data else None,
        id=data["id"] if "id" in data else None,
    )
    return {"status": "ok", "data": jsons.loads(jsons.dumps(new_user))}


@app.route('/users/<id_or_email>', methods=['DELETE'])
@auth.login_required(role='admin')
def delete_user(id_or_email):
    id, email = users_repo.delete(id_or_email=id_or_email)
    return {"status": "ok", "data": {"id": id, "e-mail": email}}


@app.errorhandler(Exception)
def handle_exception(e):
    error_name = "ERROR"
    if isinstance(e, UserWarning):
        error_name = "LOGIN_FAILED"
    if isinstance(e, IndexError):
        error_name = "INTERNAL_SERVER_ERROR"
    if isinstance(e, KeyError):
        error_name = "ITEM_NOT_FOUND"
    return {
        "status": "error",
        "message": error_name + ": " + str(e),
    }


if __name__ == '__main__':
    debug_mode = getenv('DEBUG_MODE', '1') == '1'
    http_port = int(getenv('HTTP_PORT', '8080'))
    http_host = getenv('HTTP_HOST', '0.0.0.0')
    app.run(
        debug=debug_mode,
        host=http_host,
        port=http_port
    )
