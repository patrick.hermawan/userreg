FROM python:3.9-slim

ENV PYTHONUNBUFFERED 1

WORKDIR /app/
COPY requirements.txt requirements.txt

COPY . /app/

RUN apt update  \
    && apt install -y --no-install-recommends git gcc g++ \
    && apt purge -y --auto-remove \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir --user -r requirements.txt \
    && python -m pytest userreg/tests -v \
    && rm -rf requirements.txt \
    && find /root/.local/ -follow -type f -name '*.c' -delete \
    && find /root/.local/ -follow -type f -name '*.a' -delete \
    && find /root/.local/ -follow -type f -name '*.pyc' -delete \
    && find /root/.local/ -follow -type f -name '*.js.map' -delete

EXPOSE 8080

CMD python main.py
