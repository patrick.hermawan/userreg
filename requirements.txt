Flask-HTTPAuth==4.2.0
jsons==1.4.2
mypy==0.812 # optional
pytest-testmon==1.1.0
