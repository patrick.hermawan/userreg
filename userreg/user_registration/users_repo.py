from typing import Tuple, List

import jsons

from userreg.database.db import DB
from userreg.user_registration.user import User


class UsersRepo:
    def __init__(self, db: DB):
        self.db = db
        self.db.add_index("email")
        self.db.add_index("username")
        self.db.add_index("updated_at")

    def add(self, fullname: str, email: str, password: str, role: str) -> User:
        """
        Add a user entry in the database, which includes creating a new
        (randomized) ID. Note that ID, e-mail, and username MUST be unique. This
         function also checks whether the username exists, and generates a new,
         incremental one accordingly.

        :param fullname: full name of the person
        :param email: email of the person (must be unique)
        :param password: password in plaintext
        :param role: either "admin" or "newb"

        :return: the User object with the aforementioned params
        """
        user = User(fullname=fullname, email=email, password=password, role=role)
        user.username = self.find_available_username_by_incrementing_suffix(user.username)

        self.db.insert(jsons.loads(jsons.dumps(user)))
        return user

    def find_available_username_by_incrementing_suffix(self, username: str) -> str:
        if self.db.get(username, "username"):
            username = username + "1"
            while self.db.get(username, "username"):
                username_suffix = int(username[-1]) + 1
                username = username[:-1] + str(username_suffix)
        return username

    def replace(self, fullname: str, email: str, password: str, role: str, id: str) -> User:
        """
        Replace a user with an existing ID and/or e-mail in the database.
        If an entry is "None" then it will not be updated, and the old existing
        value is used. ID cannot be changed.

        :param fullname: full name of the person
        :param email: email of the person (must be unique)
        :param password: password in plaintext
        :param role: either "admin" or "newb"
        :param id: ID of the user in 12 case-sensitive alphanumeric characters

        :return: the User object with the aforementioned params
        """
        user_dict = self.db.get(id, "id") if id else self.db.get(email, "email")

        if not user_dict:
            raise KeyError("User not found. Provide e-mail or ID")

        user = User(**user_dict)
        user.set_name(fullname) if fullname else None
        user.set_email(email) if email else None
        user.set_password(password) if password else None
        user.set_role(role) if role else None
        self.db.replace(jsons.loads(jsons.dumps(user)), user.id)

        return user

    def get(self, id_or_email_or_username) -> User:
        """
        Returns a single user from the repository

        :param id_or_email_or_username: ID, E-mail, or username. The system will
        intelligently choose the most appropriate column to search. ID consists
        of 12 case-sensitive alphanumeric characters

        :return: the User object with the aforementioned params
        """
        user_dict = self.db.get(id_or_email_or_username)

        if not user_dict:
            raise KeyError("User not found. Provide e-mail, username, or ID")

        user = User(**user_dict)
        return user

    def get_all(self) -> List[dict]:
        """
        Returns ALL users in the database

        :return: A list of user objects
        """
        all_users = self.db.get_all("updated_at")
        sorted_keys = sorted(all_users.keys(), reverse=True)
        return [all_users[i] for i in sorted_keys]

    def delete(self, id_or_email) -> Tuple[str, str]:
        """
        Deletes a user from the database

        :param id_or_email: ID, or e-mail. The system will intelligently choose
        the most appropriate column to search. ID consists of 12 case-sensitive
        alphanumeric characters

        :return: A tuple consisting of two strings: ID and e-mail
        """
        user_dict = self.db.get(id_or_email)

        if not user_dict:
            raise KeyError("User not found. Provide e-mail or ID")

        user = User(**user_dict)
        self.db.delete(user.id)

        return user.id, user.email
