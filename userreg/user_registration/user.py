import random
import string
from datetime import datetime
from typing import Tuple

from werkzeug.security import generate_password_hash


class User:
    def __init__(
            self,
            email: str,
            password: str,
            role: str,
            fullname: str,
            id: str = None,
            username: str = None,
            updated_at: str = None,
            first_name: str = None,
            middle_name: str = None,
            last_name: str = None,
            initial_name: str = None,
    ):
        if id:  # Casting an existing User from a dictionary with User(**user_dict)
            self.email = email
            self.password = password
            self.role = role
            self.fullname = fullname
            self.id = id
            self.username = username
            self.updated_at = updated_at
            self.first_name = first_name
            self.middle_name = middle_name
            self.last_name = last_name
            self.initial_name = initial_name
        else:  # Creating a new user
            self.set_email(email)
            self.set_role(role)
            self.set_name(fullname)
            self.id = generate_ascii_id(length=12)
            self.username = generate_username(self.first_name, str(self.last_name))
            self.set_password(password)
            self.set_updated_at()

    def set_name(self, fullname: str) -> None:
        validate_name(fullname)
        self.fullname = fullname
        self.initial_name = generate_initial_name(fullname)
        self.first_name, self.middle_name, self.last_name = generate_first_middle_last_name(fullname)
        self.set_updated_at()

    def set_email(self, email: str) -> None:
        validate_email(email)
        self.email = email
        self.set_updated_at()

    def set_role(self, role: str) -> None:
        validate_role(role)
        self.role = role
        self.set_updated_at()

    def set_password(self, password: str) -> None:
        validate_password(password)
        self.password = generate_password_hash(str(self.username) + password)
        self.set_updated_at()

    def set_updated_at(self) -> None:
        self.updated_at = datetime.now().isoformat()


def generate_ascii_id(length: int) -> str:
    """
    Generates random CASE-SENSITIVE string of ASCII characters, including
    numbers of a specified length

    :param length: An integer. For example: 12

    :return: A string of the specified length
    """
    return "".join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])


def validate_password(password: str) -> None:
    """
    Validates the password, throws an error if the password is invalid.

    :param password: string, the password in plaintext
    """
    if len(password) < 4:
        raise Exception("Password too short")


def validate_name(fullname: str) -> None:
    """
    Validates the name, throws an error if the name is invalid.

    :param password: string
    """
    if "@" in fullname:
        raise Exception("Your fullname is " + fullname + "? Did you put email in your name?")


def validate_email(email: str) -> None:
    """
    Validates the email, throws an error if the email is invalid.

    :param email: string
    """
    if not ("@" in email and "." in email):
        raise Exception("Email invalid")  # TODO better validation


def validate_role(role: str) -> None:
    """
    Validates the role, throws an error if the role is invalid.

    :param role: string
    """
    if not (role == "admin" or role == "newb"):
        raise Exception("Role " + role + " is invalid. Only admin and newb are supported")


def generate_initial_name(fullname: str) -> str:
    """
    Generates an initial name based on the first letter of each name of the full
    name

    For example:
    Richard M Stallman > RMS
    Rama > R
    Ebet Kadarusman > EK
    Ricardo Izecson dos Santos Leite > RIDSL
    Robert Downey, Jr > RDJ


    :param fullname: string
    :return: a string, the initial name
    """
    splitted_name_no_punctuations = fullname.translate(str.maketrans('', '', string.punctuation)).split(" ")
    return "".join([i[0].capitalize() for i in splitted_name_no_punctuations])


def generate_first_middle_last_name(fullname: str) -> Tuple[str, str, str]:
    """
    Splits the full name into separate first name, middle name, and last name.
    If the name is too short, then the middle name or last name might be an
    empty string ""

    For example:
    Deodatus Andreas Deddy Cahyadi Sunjoyo > Deodatus, Andreas Deddy Cahyadi, Sunjoyo
    Akira Toriyama > Akira, Toriyama

    :param fullname: string
    :return: a tuple which consists of the first name, middle name, and last
    name.
    """
    splitted_name = fullname.split(" ")
    return splitted_name[0], " ".join(splitted_name[1:-1]), splitted_name[-1] if len(splitted_name) > 1 else ""


def generate_username(first_name, last_name: str) -> str:
    """
    Generates a username in the format of first_name.last_name

    For example:
    Deodatus Andreas Deddy Cahyadi Sunjoyo > Deodatus, Andreas Deddy Cahyadi, Sunjoyo
    Akira Toriyama > Akira, Toriyama

    :param first_name: First name of the user (string)
    :param last_name: Last name of the user (string)

    :return: a string of the username
    """
    return (first_name + "." + last_name if last_name else first_name).lower()
