import unittest
from time import sleep

from werkzeug.security import check_password_hash

from userreg.user_registration.user import User, generate_initial_name, generate_first_middle_last_name, generate_username


class TestUser(unittest.TestCase):
    async def test_first_middle_last_name_longer_than_three_words(self):
        assert generate_first_middle_last_name(
            "Deodatus Andreas Deddy Cahyadi Sunjoyo") == (
                   "Deodatus", "Andreas Deddy Cahyadi", "Sunjoyo")

    async def test_first_middle_last_name_two_words(self):
        assert generate_first_middle_last_name("Akira Toriyama") == (
            "Akira", "", "Toriyama")

    async def test_first_middle_last_name_one_word(self):
        assert generate_first_middle_last_name("Patrick") == ("Patrick", "", "")

    async def test_initial_name_three_words(self):
        assert generate_initial_name("Richard M Stallman") == "RMS"

    async def test_initial_name_one_word(self):
        assert generate_initial_name("Rama") == "R"

    async def test_initial_name_two_words(self):
        assert generate_initial_name("Ebet Kadarusman") == "EK"

    async def test_initial_name_longer_than_three_words_with_lowercase(self):
        assert generate_initial_name(
            "Ricardo Izecson dos Santos Leite") == "RIDSL"

    async def test_initial_name_with_punctuations(self):
        assert generate_initial_name("Robert Downey, Jr") == "RDJ"

    async def test_username_one_word(self):
        assert generate_username("Rama", "") == "rama"

    async def test_username_two_words(self):
        assert generate_username("Deodatus", "Sunjoyo") == "deodatus.sunjoyo"

    async def test_role_newb(self):
        user = User(email="foo@bar.com", password="foofoo", role="newb",
                    fullname="foo")
        assert user.role == "newb"

    async def test_role_newb_then_admin(self):
        user = User(email="foo@bar.com", password="foofoo", role="newb",
                    fullname="foo")
        user.set_role("admin")
        assert user.role == "admin"

    async def test_password(self):
        user = User(email="foo@bar.com", password="password1", role="newb",
                    fullname="foo")
        assert check_password_hash(user.password, user.username + "password1")

    async def test_replace_password(self):
        user = User(email="foo@bar.com", password="password1", role="newb",
                    fullname="foo")
        user.set_password("password2")
        assert check_password_hash(user.password, user.username + "password2")

    async def test_updated_at_actually_updated(self):
        user = User(email="foo@bar.com", password="foofoo", role="newb",
                    fullname="foo")
        old_updated_at = user.updated_at

        sleep(0.01)
        user.set_updated_at()
        assert user.updated_at != old_updated_at
