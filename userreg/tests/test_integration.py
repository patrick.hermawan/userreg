import base64
import json

from unittest import TestCase

from main import app


class TestIntegration(TestCase):
    def setUp(self):
        app.testing = True
        self.client = app.test_client()

    async def auth_header(self, username, password):
        the_string = (username + ":" + password).encode('utf-8')
        user_and_pass = base64.b64encode(the_string).decode("ascii")
        return {
            'Authorization': 'Basic %s' % user_and_pass,
            "Content-Type": "application/json"
        }

    async def test_homepage(self):
        response = self.client.get(
            "/",
            headers=self.auth_header(username="admin", password="admin"),
        )
        assert response.data == b"Hello, admin!"

    async def test_get_all_users(self):
        response = self.client.get(
            "/users",
            headers=self.auth_header(username="admin", password="admin"),
        )
        assert len(response.json["data"]) == 2

    async def test_get_user(self):
        response = self.client.get(
            "/users/patrick.hermawan@outlook.com",
            headers=self.auth_header(username="patrick.hermawan",
                                     password="patrick"),
        )
        assert response.json["data"]["initial_name"] == "PH" and \
               response.json["data"]["first_name"] == "Patrick"

    async def test_insert_user(self):
        user = {
            "fullname": "Rama",
            "email": "rama@mail.com",
            "password": "rama",
            "role": "newb"
        }
        response = self.client.post(
            "/users",
            headers=self.auth_header(username="admin", password="admin"),
            data=json.dumps(user),
        )
        assert response.json["data"]["initial_name"] == "R" and \
               response.json["data"]["first_name"] == "Rama"

    async def test_update_user(self):
        user = {
            "fullname": "",
            "email": "patrick.hermawan@outlook.com",
            "password": "",
            "role": "admin"
        }
        response = self.client.patch(
            "/users",
            headers=self.auth_header(username="admin", password="admin"),
            data=json.dumps(user),
        )
        assert response.json["data"]["role"] == "admin" and \
               response.json["data"]["first_name"] == "Patrick"

    async def test_error(self):
        response = self.client.get(
            "/",
            headers=self.auth_header(username="kamu",
                                     password="passwordnyakamu"),
        )
        print(response.json["message"])
        assert response.json["status"] == "error" and \
               "LOGIN_FAILED" in response.json["message"]
