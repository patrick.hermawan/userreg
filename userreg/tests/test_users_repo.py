import unittest

from userreg.database.db import DB
from userreg.user_registration.users_repo import UsersRepo


class TestUser(unittest.TestCase):
    async def setUp(self):
        self.users_repo = UsersRepo(DB())
        self.users_repo.add(email="foo@bar.com",
                            password="foofoo",
                            role="newb",
                            fullname="foo",
                            )
        self.the_id = list(self.users_repo.db.indexes["id"].keys())[0]

    async def test_add_in_setup_method(self):
        assert len(self.users_repo.db.indexes["id"]) == 1 and \
               len(self.users_repo.db.indexes["email"]) == 1 and \
               len(self.users_repo.db.indexes["username"]) == 1 and \
               len(self.users_repo.db.indexes["updated_at"]) == 1 and \
               "foo" in self.users_repo.db.indexes["username"] and \
               "foo@bar.com" in self.users_repo.db.indexes["email"] and \
               self.users_repo.db.indexes["id"][self.the_id] == \
               self.users_repo.db.indexes["username"]["foo"] == \
               self.users_repo.db.indexes["email"]["foo@bar.com"]
    
    async def test_add_same_username(self):
        self.users_repo.add(email="fooo@bar.com", password="foofoo", role="newb", fullname="foo")
        assert "foo1" in self.users_repo.db.indexes["username"] and \
               "fooo@bar.com" in self.users_repo.db.indexes["email"]
        self.users_repo.add(email="foooo@bar.com", password="foofoo", role="newb", fullname="foo")
        assert "foo2" in self.users_repo.db.indexes["username"] and \
               "foooo@bar.com" in self.users_repo.db.indexes["email"]
    
    async def test_get_by_id(self):
        assert self.users_repo.get(id_or_email_or_username=self.the_id).id == self.the_id
    
    async def test_get_by_email(self):
        assert self.users_repo.get(id_or_email_or_username="foo@bar.com").id == self.the_id
    
    async def test_get_by_username(self):
        assert self.users_repo.get(id_or_email_or_username="foo").id == self.the_id
    
    async def test_replace_fullname(self):
        self.users_repo.replace(fullname="bar", email="", password="", role="", id=self.the_id)
        assert self.users_repo.get(self.the_id).first_name == "bar" and \
               self.users_repo.get(self.the_id).email != "" and \
               self.users_repo.get(self.the_id).role != ""
    
    async def test_replace_email(self):
        self.users_repo.replace(fullname="", email="bar@foo.com", password="", role="", id=self.the_id)
        assert self.users_repo.get("bar@foo.com").first_name != "" and \
               self.users_repo.get(self.the_id).email == "bar@foo.com" and \
               self.users_repo.get("bar@foo.com").role != ""
    
    async def test_replace_role(self):
        self.users_repo.replace(fullname="", email="", password="", role="admin", id=self.the_id)
        assert self.users_repo.get(self.the_id).first_name != "" and \
               self.users_repo.get(self.the_id).email != "" and \
               self.users_repo.get(self.the_id).role == "admin"
    
    async def test_delete_by_id(self):
        self.users_repo.delete(id_or_email=self.the_id)
        assert len(self.users_repo.db.indexes["id"]) == 0 and \
               len(self.users_repo.db.indexes["email"]) == 0 and \
               len(self.users_repo.db.indexes["username"]) == 0
    
    async def test_delete_by_email(self):
        self.users_repo.delete(id_or_email="foo@bar.com")
        assert len(self.users_repo.db.indexes["id"]) == 0 and \
               len(self.users_repo.db.indexes["email"]) == 0 and \
               len(self.users_repo.db.indexes["username"]) == 0
