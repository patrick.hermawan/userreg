from typing import Any, Union


class DB:
    def __init__(self):
        self.indexes = {"id": dict()}

    def add_index(self, index: str) -> None:
        """
        Set a specific column/attribute name as an index. INDEX MUST BE UNIQUE
        Note that the column "id" is already set as an index by default.

        :param index: The column/attribute name
        """
        an_existing_index = list(self.indexes.keys())[0]
        self.indexes[index] = {i[index]: i for i in self.indexes[an_existing_index].values()}

    def remove_index(self, index: str) -> None:
        """
        Removes a specific column/attribute name from the index.

        :param index: The column/attribute name
        """
        if len(self.indexes) == 1:
            raise Exception("If you delete " + index + " from the indexes, there would be no index left!")
        self.indexes.pop(index)

    def get_all(self, index: str = None) -> dict:
        """
        Lists the entire database entries by a specific index. If an index is
        not specified, then the index "id" will be used

        :param index: The column/attribute name (has to be already in the index)
        :return: A dictionary, where the key is the ID (or the specified index),
        and the value is the item, which is a dictionary. A dict of a dict.
        """
        return self.indexes[index] if index else self.indexes["id"]

    def get(self, query: Any, index: str = None) -> Union[None, dict]:
        """
        Get an item by a specific index. If a specific index is not given, then
        it will search all the available indexes. Note that it will NOT search
        through columns that have not been indexed!

        :param query: The search query
        :param index: The column/attribute name (has to be already in the index)

        :return: The object entry as a dictionary, or None if such item is not
        found
        """
        if index:
            return self.get_from_specific_index(query=query, index=index)
        else:
            return self.get_from_all_indexes(query=query)

    def get_from_specific_index(self, query: str, index: str) -> Union[None, dict]:
        if index not in self.indexes:
            raise IndexError("Index not found")
        if query not in self.indexes[index]:
            return None
        return self.indexes[index][query]

    def get_from_all_indexes(self, query: str) -> Union[None, dict]:
        for index in self.indexes:
            if query in self.indexes[index]:
                return self.indexes[index][query]
        return None

    def insert(self, obj: dict) -> dict:
        """
        Insert a new item in the database. INDEXED COLUMNS/ATTRIBUTES MUST BE
        UNIQUE!

        :param obj: The object in dictionary format

        :return: The very same obj
        """
        for index in self.indexes:
            if obj[index] in self.indexes[index]:  # INDEX MUST BE UNIQUE
                raise KeyError("Item already exist with " + index + ": " + obj[index])
        for index in self.indexes:
            self.indexes[index][obj[index]] = obj

        return obj

    def replace(self, new_obj: dict, key: str, specified_index: str = "id") -> dict:
        """
        Replace an item in the database. INDEXED COLUMNS/ATTRIBUTES MUST BE
        UNIQUE!

        :param new_obj: The new, replacement object in dictionary format
        :param key: The ID assigned to the object. Must be existing!
        :param specified_index: The index where the key should be searched in!

        :return: The very same new_obj
        """
        self.check_item_exist_or_exception(key=key, index=specified_index)

        old_obj = self.indexes[specified_index][key].copy()
        for index in self.indexes:
            if old_obj[index] != new_obj[index]:
                self.indexes[index][new_obj[index]] = self.indexes[index].pop(old_obj[index])

        return new_obj

    def delete(self, key: str, specified_index: str = "id") -> dict:
        """
        Delete an item from the dictionary

        :param key: The ID assigned to the object. Must be existing!
        :param specified_index: The index where the key should be searched in!

        :return: The object that was just deleted, in dictionary format
        """
        self.check_item_exist_or_exception(key=key, index=specified_index)

        obj = self.indexes[specified_index][key]
        for index in self.indexes:
            self.indexes[index].pop(obj[index])

        return obj

    def check_item_exist_or_exception(self, key, index):
        if key not in self.indexes[index]:
            raise Exception("Item " + key + " not found in " + index)
