save-env:
	pip freeze > requirements.txt
run:
	python main.py
test:
	pytest userreg/tests --ignore-glob=*test_integration.py --testmon
test-integration:
	pytest userreg/tests/test_integration.py --testmon
test-all:
	pytest userreg/tests --testmon
